pragma solidity 0.5.17;

import "./ERC20Mintable.sol";

/**
 * @dev Extension of {ERC20Mintable} that adds a cap to the supply of tokens.
 */
contract ERC20Capped is ERC20Mintable {
    uint256 private _cap;
    uint256 private _capUsed;

    uint256 private _airdropCap;
    uint256 private _airdropCapUsed;

    /**
     * @dev Sets the value of the `cap`. This value is immutable, it can only be
     * set once during construction.
     */
    constructor(uint256 cap, uint256 airdropCap) public {
        require(cap != 0, "ERC20Capped: cap is 0");
        require(airdropCap != 0, "ERC20Capped: airdrop cap is 0");
        _cap = cap;
        _airdropCap = airdropCap;
    }

    /**
     * @dev Returns the normal cap.
     */
    function cap() public view returns (uint256) {
        return _cap;
    }

    /**
     * @dev Returns the airdrop cap.
     */
    function airdropCap() public view returns (uint256) {
        return _airdropCap;
    }

    /**
     * @dev Returns the normal cap used.
     */
    function capUsed() public view returns (uint256) {
        return _capUsed;
    }

    /**
     * @dev Returns the airdrop cap used.
     */
    function airdropCapUsed() public view returns (uint256) {
        return _airdropCapUsed;
    }

    /**
     * @dev See {ERC20Mintable-mint}.
     *
     * Requirements:
     *
     * - `value` must not cause the cap used to go over the cap.
     */
    function _mint(address account, uint256 value) internal {
        uint256 newCapUsed = _capUsed.add(value);
        require(newCapUsed <= _cap, "ERC20Capped: cap exceeded");
        super._mint(account, value);
        _capUsed = newCapUsed;
    }

    /**
     * @dev See {ERC20Mintable-mint}.
     *
     * Requirements:
     *
     * - `value` must not cause the airdrop cap used to go over the airdrop cap.
     */
    function _airdropMint(address account, uint256 value) internal {
        super._mint(account, value);
    }

    /**
     * @dev Used for increasing the airdrop cap used when owner lockup the token.
     *
     * Requirements:
     *
     * - `value` must not cause the airdrop cap used to go over the cap.
     */
    function _incrementUsedAirdropCap(uint256 value) internal {
        uint256 newAirdropCapUsed = _airdropCapUsed.add(value);
        require(
            newAirdropCapUsed <= _airdropCap,
            "ERC20Capped: airdrop cap exceeded"
        );
        _airdropCapUsed = newAirdropCapUsed;
    }
}
