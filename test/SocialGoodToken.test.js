const {
  BN,
  constants,
  expectEvent,
  expectRevert,
} = require("@openzeppelin/test-helpers");
const { expect } = require("chai");
const ether = require("@openzeppelin/test-helpers/src/ether");
const { ZERO_ADDRESS } = constants;

const SocialGoodToken = artifacts.require("SocialGoodToken");

const tokenName = "SocialGood";
const tokenSymbol = "SG";
const tokenDecimals = new BN(18);
const normalHardCap = ether("209250000");
const airdropHardCap = ether("750000");

contract("SocialGoodToken", function ([
  owner,
  otherAccount,
  mintAccount,
  ...migrationAccounts
]) {
  beforeEach(async function () {
    this.token = await SocialGoodToken.new({
      from: owner,
    });
  });

  it("has a name", async function () {
    expect(await this.token.name()).to.equal(tokenName);
  });

  it("has a symbol", async function () {
    expect(await this.token.symbol()).to.equal(tokenSymbol);
  });

  it("has an amount of decimals", async function () {
    expect(await this.token.decimals()).to.be.bignumber.equal(tokenDecimals);
  });

  it("equal initiliazed hard cap", async function () {
    expect(await this.token.cap()).to.be.bignumber.equal(normalHardCap);
  });

  describe("burn", function () {
    const ownerMintAmount = new BN(100);
    const otherAccountMintAmount = new BN(10);
    const burnAmount = new BN(1);

    beforeEach(async function () {
      await this.token.mint(owner, ownerMintAmount, {
        from: owner,
      });
      await this.token.mint(otherAccount, otherAccountMintAmount, {
        from: owner,
      });
    });

    describe("when caller is owner", function () {
      it("can burn your own token", async function () {
        await this.token.burn(burnAmount, {
          from: owner,
        });
        expect(await this.token.balanceOf(owner)).to.be.bignumber.equal(
          ownerMintAmount.sub(burnAmount)
        );
      });
    });

    describe("when caller is not owner", function () {
      describe("cannot burn your own token", function () {
        it("reverts", async function () {
          await expectRevert(
            this.token.burn(burnAmount, {
              from: otherAccount,
            }),
            "Ownable: caller is not the owner"
          );
        });
      });
    });
  });

  describe("burnFrom", function () {
    const ownerMintAmount = new BN(100);
    const otherAccountMintAmount = new BN(10);
    const burnAmount = new BN(1);

    beforeEach(async function () {
      await this.token.mint(owner, ownerMintAmount, {
        from: owner,
      });
      await this.token.mint(otherAccount, otherAccountMintAmount, {
        from: owner,
      });
    });

    describe("when caller is owner", function () {
      it("can burn token of any address", async function () {
        await this.token.approve(owner, burnAmount, {
          from: otherAccount,
        });
        await this.token.burnFrom(otherAccount, burnAmount, {
          from: owner,
        });
        expect(await this.token.balanceOf(otherAccount)).to.be.bignumber.equal(
          otherAccountMintAmount.sub(burnAmount)
        );
      });
    });

    describe("when caller is not owner", function () {
      describe("cannot burn token of any address", function () {
        it("reverts", async function () {
          await this.token.approve(otherAccount, burnAmount, {
            from: owner,
          });
          await expectRevert(
            this.token.burnFrom(owner, burnAmount, {
              from: otherAccount,
            }),
            "Ownable: caller is not the owner"
          );
        });
      });
    });
  });

  describe("airdropLockup", function () {
    let accounts;
    let amounts;

    beforeEach(function () {
      accounts = migrationAccounts.slice(0, 5);
      amounts = Array.from(Array(5).keys()).map((i) => {
        return new BN(i + 1).muln(10);
      });
    });

    describe("when caller is owner", function () {
      it("can lockup token for multi accounts", async function () {
        await this.token.airdropLockup(accounts, amounts, {
          from: owner,
        });
        for (const [index, account] of accounts.entries()) {
          const lockupAmount = await this.token.getAddressLockupAmount(account);
          expect(lockupAmount).to.be.bignumber.equal(amounts[index]);
        }
      });

      it("can lockup token if airdrop cap used less than airdrop cap", async function () {
        await this.token.airdropLockup(
          [accounts[0]],
          [airdropHardCap.subn(1)],
          {
            from: owner,
          }
        );
        const lockupAmount = await this.token.getAddressLockupAmount(
          accounts[0]
        );
        expect(lockupAmount).to.be.bignumber.equal(airdropHardCap.subn(1));
      });

      describe("should fail if accounts & amounts length are not the same", function () {
        it("reverts", async function () {
          accounts.push(migrationAccounts[5]);
          await expectRevert(
            this.token.airdropLockup(accounts, amounts, {
              from: owner,
            }),
            "SocialGoodToken: the length of accounts, amounts are not the same."
          );
        });
      });

      describe("should fail if recepient address is zero address", function () {
        it("reverts", async function () {
          accounts.push(ZERO_ADDRESS);
          amounts.push(new BN(1));

          await expectRevert(
            this.token.airdropLockup(accounts, amounts, {
              from: owner,
            }),
            "SocialGoodToken: airdrop target address is empty"
          );
        });
      });

      describe("should fail if recepient token amount is zero", function () {
        if (
          ("reverts",
          async function () {
            accounts.push(migrationAccounts[5]);
            amounts.push(new BN(0));

            await expectRevert(
              this.token.airdropLockup(accounts, amounts, {
                from: owner,
              }),
              "SocialGoodToken: amount is zero"
            );
          })
        );
      });

      describe("should fail if lockup amount exceeds the airdrop cap", function () {
        it("reverts", async function () {
          await this.token.airdropLockup(
            [mintAccount],
            [airdropHardCap.subn(1)],
            {
              from: owner,
            }
          );
          await expectRevert(
            this.token.airdropLockup([mintAccount], [2], {
              from: owner,
            }),
            "ERC20Capped: airdrop cap exceeded"
          );
        });
      });

      describe("should fail when lockup after airdrop cap is reached", function () {
        it("reverts", async function () {
          await this.token.airdropLockup([mintAccount], [airdropHardCap], {
            from: owner,
          });
          await expectRevert(
            this.token.airdropLockup([mintAccount], [1], {
              from: owner,
            }),
            "ERC20Capped: airdrop cap exceeded"
          );
        });
      });
    });

    describe("when caller is not owner", function () {
      describe("cannot airdrop tokens", function () {
        it("reverts", async function () {
          await expectRevert(
            this.token.airdropLockup(accounts, amounts, {
              from: otherAccount,
            }),
            "Ownable: caller is not the owner"
          );
        });
      });
    });

    describe("when paused", function () {
      describe("cannot airdrop tokens", function () {
        it("reverts", async function () {
          await this.token.pause({
            from: owner,
          });
          await expectRevert(
            this.token.airdropLockup(accounts, amounts, {
              from: owner,
            }),
            "Pausable: paused"
          );
        });
      });
    });
  });

  describe("unlocks", function () {
    let accounts;
    let amounts;

    beforeEach(async function () {
      accounts = migrationAccounts.slice(0, 5);
      amounts = Array.from(Array(5).keys()).map((i) => {
        return new BN(i + 1).muln(10);
      });

      await this.token.airdropLockup(accounts, amounts, {
        from: owner,
      });
    });

    describe("when caller is owner", function () {
      it("can unlock token", async function () {
        await this.token.unlocks(accounts, {
          from: owner,
        });
        for (const [index, account] of accounts.entries()) {
          const balance = await this.token.balanceOf(account);
          expect(balance).to.be.bignumber.equal(amounts[index]);
        }
      });

      describe("cannot unlock when account has not locked up", function () {
        it("reverts", async function () {
          accounts.push(migrationAccounts[5]);
          await expectRevert(
            this.token.unlocks(accounts, {
              from: owner,
            }),
            "SocialGoodToken: amount is zero"
          );
        });
      });

      describe("cannot unlock when account has already unlocked", function () {
        it("reverts", async function () {
          await this.token.unlocks(accounts, {
            from: owner,
          });
          await expectRevert(
            this.token.unlocks(accounts, {
              from: owner,
            }),
            "SocialGoodToken: amount is zero"
          );
        });
      });
    });

    describe("when caller is not owner", function () {
      it("reverts", async function () {
        await expectRevert(
          this.token.unlocks(accounts, {
            from: otherAccount,
          }),
          "Ownable: caller is not the owner"
        );
      });
    });

    describe("when paused", function () {
      it("reverts", async function () {
        await this.token.pause({
          from: owner,
        });
        await expectRevert(
          this.token.unlocks(accounts, {
            from: owner,
          }),
          "Pausable: paused"
        );
      });
    });
  });
});
