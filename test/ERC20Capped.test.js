const { BN, ether, expectRevert } = require("@openzeppelin/test-helpers");
const {
  shouldBehaveLikeERC20Mintable,
} = require("./behaviors/ERC20Mintable.behavior");
const {
  shouldBehaveLikeERC20Capped,
} = require("./behaviors/ERC20Capped.behavior");

const ERC20Capped = artifacts.require("ERC20Capped");

contract("ERC20Capped", function ([minter, ...otherAccounts]) {
  const cap = ether("1000");
  const airdropCap = ether("100");

  it("requires a non-zero cap", async function () {
    await expectRevert(
      ERC20Capped.new(new BN(0), airdropCap, {
        from: minter,
      }),
      "ERC20Capped: cap is 0"
    );
  });

  it("requires a non-zero airdrop cap", async function () {
    await expectRevert(
      ERC20Capped.new(cap, new BN(0), {
        from: minter,
      }),
      "ERC20Capped: airdrop cap is 0"
    );
  });

  context("once deployed", async function () {
    beforeEach(async function () {
      this.token = await ERC20Capped.new(cap, airdropCap, {
        from: minter,
      });
    });

    shouldBehaveLikeERC20Capped(minter, otherAccounts, cap, airdropCap);
    shouldBehaveLikeERC20Mintable(minter, otherAccounts);
  });
});
